import java.util.*;

// viide, kust kood oli võetud ning uuritud: https://enos.itcollege.ee/~ylari/I231/Huffman.java?fbclid=IwAR2-3gEiuldCk_GtysjB0AL1Esi_Rhk0zqH6SrgRkiIDeJYj4p6wP1IrM1Y

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private int[] frequencies;

   private Map<Byte, Leaf> table;

   private int length;

   /** Constructor to build the Huffman code for a given byte array.
    * Creates tree in constructor.
    * @param original source data
    */
   Huffman (byte[] original) {
      frequencies = new int[256];
      length = 0;
      table = new HashMap<Byte, Leaf>();
      init(original);
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return length;
   }


   /** Encode given byte array using Huffman algorithm.
    * Encodes data using tree.
    * @param origData is converted to byte array string.
    * @return encoded byte array
    */
   public byte[] encode (byte [] origData) {
      StringBuilder sb = new StringBuilder();
      for (byte character: origData) { // append string with bits, that are created by Huffman constructor
         sb.append(table.get(character).code);
      }
      length = sb.length(); // bit length of data is string length
      byte[] bytes = new byte[sb.length()/8 + 1];
      int counter = 0;
      while(sb.length() > 0){ // while there is data
         while(sb.length()<8){ // if string is smaller than 8, then append string with 0 to receive byte length
            sb.append("0");
         }
         String str = sb.substring(0, 8); // take byte
         bytes[counter] = (byte)Integer.parseInt(str, 2); // add byte to byte array
         counter++; // append counter
         sb.delete(0,8); // delete used byte
      }
      return bytes; // return byte array
   }

   /** Decode encoded byte array.
    * @param encodedData is encoded byte array
    * @return decoded byte array
    */
   public byte[] decode (byte[] encodedData) {
      StringBuilder sb = new StringBuilder();
      for(int i = 0; i < encodedData.length; i++){
         // append string with binary value that is converted from encoded data. "%8s" - fills with spaces untill length 10 is received. 0xFF - logical multiplication
         sb.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
      }
      String encodedStr = sb.toString().substring(0, length); // delete unnecessary 0s
      LinkedList<Byte> bytes = new LinkedList<>();
      StringBuilder code = new StringBuilder();
      while (encodedStr.length() > 0){ // while there are still elements
         code.append(encodedStr.substring(0, 1)); // append code with one bit
         encodedStr = encodedStr.substring(1); // delete bit already taken to code
         for (Leaf leaf : table.values()) { // take every leaf from table
            if (leaf.code.equals(code.toString())) { // if leaf code equals to code that is watched now
               bytes.add(leaf.value); // add to byte list value of leaf
               code.delete(0, code.length()); // empty code
               break;
            }
         }
      }
      byte[] result = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++){ // convert list to array
         result[i] = bytes.get(i);
      }
      return result; // return byte array
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }

   /** Converts bytes to tree leafs according to their frequency.
    * @param data is byte array that needs to be converted to tree
    */
   private void init(byte[] data){
      for (byte charachter : data) {
         frequencies[charachter]++; // increase frequencies of elements in the frequency list
      }
      PriorityQueue<Tree> trees = new PriorityQueue<>(); // create queue where byte elements will be added
      for(int i = 0; i < frequencies.length; i++){
         if(frequencies[i] > 0){ // if element appears in given byte array at least once, add it to the queue in right place as a Leaf instance
            trees.offer(new Leaf(frequencies[i], (byte) i));
         }
      }
      if(trees.size() == 0){ throw  new RuntimeException("Empty string"); }
      while (trees.size()>1){
         trees.offer(new Subtree(trees.poll(),trees.poll())); // poll() takes header of the queue, which is smallest frequency
                                                              // subtree has frequency as a sum of inserted values friquencies
      }
      code(trees.poll(), new StringBuilder());
   }

   /** Creates code for subtrees and leafs according to their location in tree.
    * @param tree currently examined tree (subtree)
    * @param prefix is code of leaf or subtree
    */
   private void code(Tree tree, StringBuilder prefix) {
      if(tree == null){throw new RuntimeException("Tree is empty!"); }
      if (tree instanceof Leaf) { // if Leaf element is reached - it's prefix code is read and written down into HashMap "table"
         Leaf leaf = (Leaf) tree;
         leaf.code = (prefix.length() > 0) ? prefix.toString() : "0";
         table.put(leaf.value, leaf);
      } else if (tree instanceof Subtree) { // if element is not Leaf element, the continue digging further
         Subtree subtree = (Subtree) tree; // take current subtree as a subtree instance
         prefix.append('0'); // append prefix code with 0, take left subtree and create a tree recursion
         code(subtree.left, prefix);
         prefix.deleteCharAt(prefix.length() - 1); // delete last char in prefix to take right subtree, which frefix ends with 1
         prefix.append('1');
         code(subtree.right, prefix);
         prefix.deleteCharAt(prefix.length() - 1); // delete last char to avoid doublication of prefix
      }
   }

   /**Parent tree that represents Huffman class that implements Comparable<> class.
    * Is needed to provide appropriate work to queue, as class frequency elements will become comparable.
    * Overrides compareTo() method from Comparable<> class
    */
   abstract class Tree implements Comparable<Tree>{

      public int frequency;

      public Tree(int frequency){
         this.frequency = frequency;
      }

      @Override
      public int compareTo(Tree tree){
         return frequency - tree.frequency;
      }
   }

   // Leaf of tree that carries the value of leaf and its frequency
   class Leaf extends Tree{

      public byte value;

      public String code;

      public Leaf(int frequency, byte value){
         super(frequency);
         this.value = value;
      }
   }

   // Subtree of main tree that carries frequency of its leafs or subtrees
   class Subtree extends Tree{

      public Tree left, right;

      public Subtree(Tree left, Tree right){
         super(left.frequency + right.frequency);
         this.left = left;
         this.right = right;
      }
   }
}

